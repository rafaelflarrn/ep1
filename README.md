# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto
Tentativa de implementação de uma urna eletrônica. Projeto incompleto. Não foram implementadas as funcionalidades para contar os votos ou para mostrar o vencedor.
## Bugs e problemas
O projeto possui falha de compilação.
## Referências
