#include "candidato.hpp"
using namespace std;

Candidato::Candidato(){
	cargo = "";
	partido = "";
	estado = "";
	num_candidato = 0;
}
Candidato::~Candidato(){}

string Candidato::get_cargo(){
	return cargo;
}
void Candidato::set_cargo(string cargo){
	this->cargo = cargo;
}
string Candidato::get_partido(){
	return partido;
}
void Candidato::set_partido(string partido){
	this->partido = partido;
}
string Candidato::get_estado(){
	return estado;
}
void Candidato::set_estado(string estado){
	this->estado = estado;
}
int Candidato::get_num_candidato(){
	return num_candidato;
}
void Candidato::set_num_candidato(int num_candidato){
	this->num_candidato = num_candidato;
}

void Candidato::imprime_dados(){
	cout << "Candidato a " << cargo;
}









