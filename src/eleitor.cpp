#include "eleitor.hpp"
#include <iostream>

using namespace std;

Eleitor::Eleitor(){
	voto_pres = 0;
	voto_gov = 0;
	voto_sen = 0;
	voto_fed = 0;
	voto_dis = 0;
	num_eleitor = 0;
}
Eleitor::~Eleitor(){}

void Eleitor::set_voto_pres(int voto_pres){
	this->voto_pres = voto_pres;
}
int Eleitor::get_voto_pres(){
	return voto_pres;
}
void Eleitor::set_voto_gov(int voto_gov){
	this->voto_gov = voto_gov;
}
int Eleitor::get_voto_gov(){
	return voto_gov;
}
void Eleitor::set_voto_sen(int voto_sen){
	this->voto_sen = voto_sen;
}
int Eleitor::get_voto_sen(){
	return voto_sen;
}
void Eleitor::set_voto_fed(int voto_fed){
	this->voto_fed = voto_fed;
}
int Eleitor::get_voto_fed(){
	return voto_fed;
}
void Eleitor::set_voto_dis(int voto_dis){
	this->voto_dis = voto_dis;
}
int Eleitor::get_voto_dis(){
	return voto_dis;
}
void Eleitor::set_num_eleitor(int num_eleitor){
	this->num_eleitor = num_eleitor;
}
int Eleitor::get_num_eleitor(){
	return num_eleitor;
}

void Eleitor::imprime_dados(){
	cout << "\t" << voto_pres << "\t" << voto_gov;
}

