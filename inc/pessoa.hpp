#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>
#endif

using namespace std;

class Pessoa{

	private:
		string nome;
	public:
		Pessoa();
		~Pessoa();
		string get_nome();
		void set_nome(string nome);
		
};
