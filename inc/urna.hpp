#ifndef URNA_HPP
#define URNA_HPP
#include "eleitor.hpp"
#include "candidato.hpp"
#include <iostream>
#endif

using namespace std;

class Urna{
	private:
		int eleitores;
		Urna();
	public:
		Urna(int eleitores);
		~Urna();
		int get_eleitores();
		void set_eleitores(int eleitores);

		virtual void process();
};
