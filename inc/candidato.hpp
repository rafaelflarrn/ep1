#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <iostream>
#include "pessoa.hpp"
#endif

using namespace std;

class Candidato{
	private:
		string cargo;
		string partido;
		string estado;
		int num_candidato;
		Candidato();
	public:
		Candidato(string nome, string cargo, string partido, string estado, int num_candidato);
		~Candidato();
		
		string get_cargo();
		void set_cargo(string cargo);
		string get_partido();
		void set_partido(string partido);
		string get_estado();
		void set_estado(string estado);
		int get_num_candidato();
		void set_num_candidato(int num_candidato);

		virtual void imprime_dados();
};

