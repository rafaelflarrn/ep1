#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include <string>
#include "pessoa.hpp"
#endif

using namespace std;

class Eleitor : public Pessoa{

	private:
		int voto_pres;
		int voto_sen;
		int voto_gov;
		int voto_fed;
		int voto_dis;
		int num_eleitor;
		Eleitor();
	public:
		Eleitor(string nome, int voto_pres, int voto_sen,int voto_gov, int voto_fed, int voto_dis, int num_eleitor);
		~Eleitor();
		int get_voto_pres();
		void set_voto_pres(int voto_pres);
		int get_voto_gov();
		void set_voto_gov(int voto_gov);
		int get_voto_sen();
		void set_voto_sen(int voto_sen);
		int get_voto_dis();
		void set_voto_dis(int voto_dis);		
		int get_voto_fed();
		void set_voto_fed(int voto_fed);	
		int get_num_eleitor();
		void set_num_eleitor(int num_eleitor);
		virtual void imprime_dados();
};


